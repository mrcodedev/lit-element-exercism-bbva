import {
  LitElement,
  html
} from 'lit-element';

export class LitElementPropAttrTemplate extends LitElement {
  render() {
    return html `
      <h1>${this.title}</h1>
      <p>${this.text}</p>`;
  }
  static get properties() {
    return {
      title: {
        type: String,
        attribute: 'comp-title'
      },
      text: {
        type: String,
        attribute: 'comp-text'
      }
    }
  }

  constructor() {
    super();
  }
}

customElements.define('lit-element-prop-attr-template', LitElementPropAttrTemplate);