/**
 * @license
 * Copyright (c) 2017 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
  var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
  else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { LitElementPropAttrTemplate } from '../src/lit-element-prop-attr-template.js';
const {expect} = chai;
// // tslint:disable:no-any ok in tests
suite('LitElement', () => {
let container;
setup(() => {
  container = document.createElement('div');
  document.body.appendChild(container);
  const el = new LitElementPropAttrTemplate();
  el.title = 'hi from Lit element';
  el.text = 'this is a test';
  container.appendChild(el);
});
teardown(() => {
  if (container && container.parentNode) {
    container.parentNode.removeChild(container);
  }
});
test('expect a render of h1: "hi from Lit element" and p: "this is a test"', async () => {
  await new Promise((resolve) => {
    setTimeout(() => {
      const el = container.querySelector('lit-element-prop-attr-template').shadowRoot;
      const h1 = el.querySelector('h1');
      const p = el.querySelector('p');
      expect(h1).to.not.null
      expect(h1.innerText).to.be.equal('hi from Lit element');
      expect(p).to.not.null
      expect(p.innerText).to.be.equal('this is a test');
      resolve();
    });
  });
});
});