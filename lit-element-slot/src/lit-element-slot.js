import {
  LitElement,
  html
} from 'lit-element';

export class LitElementSlot extends LitElement {

  render() {
    return html `
      <slot></slot>
    `;
  }

}

customElements.define('lit-element-slot', LitElementSlot);