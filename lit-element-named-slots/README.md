# Lit Element - named and no named slot
Using [Lit Element 1.x](https://lit-element.polymer-project.org/)

First create a ```src``` folder and put a ```lit-element-slot.js``` file.

The Component use three slots:
1 - with name **first**
2 - with name **second**
3 - no named slot

So you can use to add ```html``` to any, using his name o no using name is added to the slot with no name.

## Setup

Go through the setup instructions for Javascript to install the necessary
dependencies:

[https://exercism.io/tracks/javascript/installation](https://exercism.io/tracks/javascript/installation)

## Requirements

Install assignment dependencies:

```bash
$ npm install 
```

## Making the test suite pass

Execute the tests with:

```bash
$ npm test
```


## Submitting Incomplete Solutions

It's possible to submit an incomplete solution so you can see how others have
completed the exercise.