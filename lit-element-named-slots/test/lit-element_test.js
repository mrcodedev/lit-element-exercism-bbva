/**
 * @license
 * Copyright (c) 2017 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
  var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
  else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { html as htmlWithStyles, render} from '../lit-element.js';
import { LitElementNamedSlots } from '../src/lit-element-named-slots.js';
import { generateElementName, stripExpressionDelimeters } from './test-helpers.js';
const {expect} = chai;
// // tslint:disable:no-any ok in tests
suite('LitElement', () => {
  let container;
  const hi = 'Hello';
  const textFirstSlot = 'Hello FIRST slot';
  const textSecondSlot = 'Hello SECOND slot';
  const textNoNamedSlot = 'Hello no named slot';
  setup(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
    const el = new LitElementNamedSlots();
    container.appendChild(el);
  });
  teardown(() => {
    if (container && container.parentNode) {
      container.parentNode.removeChild(container);
    }
  });
  test('expect slot with no name got this content "<p>Hello no named slot : </p> tHello no named slot <p>', async () => {
    await new Promise((resolve) => {
      setTimeout(() => {
        const cont = htmlWithStyles `<p>${textNoNamedSlot}</p>`;
        const [slot] = container.querySelector('lit-element-named-slots').shadowRoot.querySelectorAll('slot:not([name])');
        render(cont, slot)
        const p = container.querySelector('lit-element-named-slots').shadowRoot.querySelectorAll('p');
        const [noNamedP] = p;
        expect(p.length).to.be.equal(1);
        expect(noNamedP).to.not.be.null;
        expect(noNamedP.innerText).to.be.equal(textNoNamedSlot);
        resolve();
      });
    });
  });
  test('expect slot with [name=first] got this content "<p>Hello no named slot : </p>Hello FIRST slot<p>', async () => {
    await new Promise((resolve) => {
      setTimeout(() => {
        const contFirst = htmlWithStyles `<p name="first">${textFirstSlot}</p>`;
        const content = container.querySelector('lit-element-named-slots').shadowRoot;
        render(contFirst, content);
        const p = content.querySelectorAll('p');
        const [second] = p;
        expect(p.length).to.be.equal(1);
        expect(second.innerText).to.be.equal(textFirstSlot);
        resolve();
      });
    });
  });
  test('expect slot with [name=second] got this content "<p slot="second">Hello no named slot : </p>Hello SECOND slot<p>', async () => {
    await new Promise((resolve) => {
      setTimeout(() => {
        const contSecond = htmlWithStyles `<p slot="second">${textSecondSlot}</p>`;
        const content = container.querySelector('lit-element-named-slots').shadowRoot;
        render(contSecond, content);
        const p = content.querySelectorAll('p');
        const [second] = p;
        expect(p.length).to.be.equal(1);
        expect(second.innerText).to.be.equal(textSecondSlot);
        resolve();
      });
    });
  });
  test('expect slot with [name=first] got this content  slot : </p>Hello FIRST slot<p>\
  slot with no name got this content "<p>Hello no named slot : </p> tHello no named slot <p>', async () => {
    await new Promise((resolve) => {
      setTimeout(() => {
        const contSecond = htmlWithStyles `<p slot="first">${textFirstSlot}</p><p>${textNoNamedSlot}</p>`;
        const content = container.querySelector('lit-element-named-slots').shadowRoot;
        render(contSecond, content);
        const p = content.querySelectorAll('p');
        const [first, noname] = p;
        expect(p.length).to.be.equal(2);
        expect(first.innerText).to.be.equal(textFirstSlot);
        expect(noname.innerText).to.be.equal(textNoNamedSlot);
        resolve();
      });
    });
  });
  test('expect slot with [name=first] got this content slot : </p>Hello FIRST slot<p>\
   slot with [name=second] got this content  slot : </p>Hello SECOND slot<p>', async () => {
    await new Promise((resolve) => {
      setTimeout(() => {
        const contSecond = htmlWithStyles `<p slot="first">${textFirstSlot}</p><p slot="second">${textSecondSlot}</p>`;
        const content = container.querySelector('lit-element-named-slots').shadowRoot;
        render(contSecond, content);
        const p = content.querySelectorAll('p');
        const [first, second] = p;
        expect(p.length).to.be.equal(2);
        expect(first.innerText).to.be.equal(textFirstSlot);
        expect(second.innerText).to.be.equal(textSecondSlot);
        resolve();
      });
    });
  });
  test('expect slot with [name=first] got this content slot : </p>Hello FIRST slot<p>\
   slot with [name=second] got this content  slot : </p>Hello SECOND slot<p>\
   slot with [name=second] got this content  slot : </p>Hello SECOND slot<p>', async () => {
    await new Promise((resolve) => {
      setTimeout(() => {
        const contSecond = htmlWithStyles `<p slot="first">${textFirstSlot}</p><p slot="second">${textSecondSlot}</p><p>${textNoNamedSlot}</p>`;
        const content = container.querySelector('lit-element-named-slots').shadowRoot;
        render(contSecond, content);
        const p = content.querySelectorAll('p');
        const [first, second, noname] = p;
        expect(p.length).to.be.equal(3);
        expect(first.innerText).to.be.equal(textFirstSlot);
        expect(second.innerText).to.be.equal(textSecondSlot);
        expect(noname.innerText).to.be.equal(textNoNamedSlot);
        resolve();
      });
    });
  });
});