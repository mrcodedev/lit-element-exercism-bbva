import {
  LitElement,
  html
} from 'lit-element';

export class LitElementSlot extends LitElement {
  render() {
    return html `
    <slot name="first"></slot>
    <slot name="second"></slot>
    <slot></slot>
    `;
  }
}

customElements.define('lit-element-named-slots');