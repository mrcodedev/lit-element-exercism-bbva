# Lit Element - Template with html style
Using [Lit Element 1.x](https://lit-element.polymer-project.org/)

First create a ```src``` folder and put a ```lit-element-template-style.js``` file.
This file will use the method **render** to get the html output.
In this test wee  will work with style [lit-html](https://lit-html.polymer-project.org).

 Setting a prop to set the **color** (spanColor) and **text-decoration** (spanTextDecoration) of the ```span``` it has defined **font-size** of 30px. And setting ```h1``` and ```p``` **color** and **font-size**. And for the ```component```  **display*** and **background-color**.


## Setup

Go through the setup instructions for Javascript to install the necessary
dependencies:

[https://exercism.io/tracks/javascript/installation](https://exercism.io/tracks/javascript/installation)

## Requirements

Install assignment dependencies:

```bash
$ npm install 
```

## Making the test suite pass

Execute the tests with:

```bash
$ npm test
```


## Submitting Incomplete Solutions

It's possible to submit an incomplete solution so you can see how others have
completed the exercise.