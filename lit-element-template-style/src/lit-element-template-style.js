import {
  LitElement,
  html,
  css
} from 'lit-element';

export class LitElementTemplateStyle extends LitElement {

  static get styles() {
    return css `
    :host {
      display: block;
      background-color: red;
    }

    h1 {
      color: yellow;
      font-size: 24px;
    }

    p {
      color: blue;
      font-size: 18px
    }

    span {
      font-size: 30px;
    }
    `;
  }

  render() {
    return html `
    <style>
    span {
      text-decoration: ${this.spanTextDecoration};
      color: ${this.spanColor};
    }
    </style>
    <h1><p><span>Texto</span></p></h1>
    `;
  }

  static get properties() {
    return {
      spanColor: {
        type: String,
        attribute: 'color',
        reflect: true
      },
      spanTextDecoration: {
        type: String
      }
    }
  }

  constructor() {
    super();
    this.spanTextDecoration = "underline";
  }
}

customElements.define('lit-element-template-style', LitElementTemplateStyle); 