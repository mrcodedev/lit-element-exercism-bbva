/**
 * @license
 * Copyright (c) 2017 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
  var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
  else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { LitElementTemplateStyle } from '../src/lit-element-template-style.js';
import { generateElementName, stripExpressionDelimeters } from './test-helpers.js';
const {expect} = chai;
// // tslint:disable:no-any ok in tests
suite('LitElement', () => {
  let container;
  const getStyle = el => prop => window.getComputedStyle(el).getPropertyValue(prop);
  setup(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
    const el = new LitElementTemplateStyle();
    el.color = 'orange';
    el.decoration = 'underline';
    container.appendChild(el);
  });
  teardown(() => {
    if (container && container.parentNode) {
      container.parentNode.removeChild(container);
    }
  });
  test('expect :host style is display: block, and backgroud-color: red; ', async () => {
    await new Promise((resolve) => {
      setTimeout(() => {
        const el = container.querySelector('lit-element-template-style');
        const getPropertyValue = getStyle(el);
        expect(getPropertyValue('background-color')).to.be.equal('rgb(255, 0, 0)')
        expect(getPropertyValue('display')).to.be.equal('block')
        resolve();
      });
    });
  });
  test('expect h1 style is color: yellow, and font-size: 24px; ', async () => {
    await new Promise((resolve) => {
      setTimeout(() => {
        const h1 = container.querySelector('lit-element-template-style').shadowRoot.querySelector('h1');
        const getPropertyValue = getStyle(h1);
        expect(getPropertyValue('color')).to.be.equal('rgb(255, 255, 0)')
        expect(getPropertyValue('font-size')).to.be.equal('24px')
        resolve();
      });
    });
  });
  test('expect p style is color: blue, and font-size: 18px; ', async () => {
    await new Promise((resolve) => {
      setTimeout(() => {
        const p = container.querySelector('lit-element-template-style').shadowRoot.querySelector('p');
        const getPropertyValue = getStyle(p);
        expect(getPropertyValue('color')).to.be.equal('blue')
        expect(getPropertyValue('font-size')).to.be.equal('18px')
        resolve();
      });
    });
  });
  test('expect span style is color: orange, and text-decoration: underline; font-size: 30px; ', async () => {
    await new Promise((resolve) => {
      setTimeout(() => {
        const span = container.querySelector('lit-element-template-style').shadowRoot.querySelector('p').querySelector('span');
        const getPropertyValue = getStyle(span);
        expect(getPropertyValue('color')).to.be.equal('orange')
        expect(getPropertyValue('text-decoration')).to.be.equal('underline')
        expect(getPropertyValue('font-size')).to.be.equal('30px')
        resolve();
      });
    });
  });
});