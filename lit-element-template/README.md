# Lit Element - Template with html
Using [Lit Element 1.x](https://lit-element.polymer-project.org/)

First create a ```src``` folder and put a ```lit-element-template.js``` file.
This file will use the method **render** to get the html output.


## Setup

Go through the setup instructions for Javascript to install the necessary
dependencies:

[https://exercism.io/tracks/javascript/installation](https://exercism.io/tracks/javascript/installation)

## Requirements

Install assignment dependencies:

```bash
$ npm install 
```

## Making the test suite pass

Execute the tests with:

```bash
$ npm test
```


## Submitting Incomplete Solutions

It's possible to submit an incomplete solution so you can see how others have
completed the exercise.