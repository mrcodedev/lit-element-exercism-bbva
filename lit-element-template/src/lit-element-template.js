import {
  LitElement,
  html
} from 'lit-element';

export class LitElementTemplate extends LitElement {
  render() {
    return html `<h1>Lit Element</h1><p>Is only a template</p>`;
  }
}

customElements.define('lit-element-template', LitElementTemplate)