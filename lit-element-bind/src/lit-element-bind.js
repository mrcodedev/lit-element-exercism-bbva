import {
  LitElement,
  html
} from 'lit-element';

export class LitElementBind extends LitElement {
  render() {
    return html `
    <style>
    .error {
      color: red;
    }

    .warning {
      color: orange;
    }

    [hidden] {
      display: none;
    }

    </style>
    <div>
      <h1 class="${this.clase}" ?hidden="${this.isHidden}">${this.title}</h1>
      <input .value=${this.value} />
      </div>
    `;
  }

  static get properties() {
    return {
      title: {
        type: String,
        attribute: 'comp-title'
      },
      clase: {
        type: String,
        attribute: 'comp-clase'
      },
      value: {
        type: String
      },
      isHidden: {
        type: Boolean,
      },
    }
  }

  constructor() {
    super();
  }
}

customElements.define('lit-element-bind', LitElementBind);