/**
 * @license
 * Copyright (c) 2017 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
  var c = arguments.length,
    r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
    d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
  else
    for (var i = decorators.length - 1; i >= 0; i--)
      if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import {
  LitElementBind
} from '../src/lit-element-bind.js';
import {
  generateElementName,
  stripExpressionDelimeters
} from './test-helpers.js';
const {
  expect
} = chai;
// // tslint:disable:no-any ok in tests
suite('LitElement', () => {
  let container;
  const getStyle = el => prop => window.getComputedStyle(el).getPropertyValue(prop);
  setup(() => {
    container = document.createElement('div');
    document.body.appendChild(container);

  });
  teardown(() => {
    if (container && container.parentNode) {
      container.parentNode.removeChild(container);
    }
  });
  test('1Bind to an attribute: expect h1 class to be error when is defined as attribute', async () => {
    const el = new LitElementBind();
    const title = "yet not modified";
    el.setAttribute('comp-clase', 'error');
    el.setAttribute('comp-title', title);
    container.appendChild(el);
    await new Promise((resolve) => {
      setTimeout(() => {
        const h1 = container.querySelector('lit-element-bind').shadowRoot.querySelector('h1');
        expect(h1.innerText).to.be.equal('yet not modified');
        expect(getStyle(h1)('color')).to.be.equal('rgb(255, 0, 0)')
        resolve();
      });
    });
  });
  test('2Bind to text content: expect h1 class to be error when is defined as attribute', async () => {
    const el = new LitElementBind();
    const title = 'bind to text content';
    el.setAttribute('comp-clase', 'warning');
    el.setAttribute('comp-title', title);
    container.appendChild(el);
    await new Promise((resolve) => {
      setTimeout(() => {
        const h1 = container.querySelector('lit-element-bind').shadowRoot.querySelector('h1');
        expect(h1.innerText).to.be.equal(title);
        expect(getStyle(h1)('color')).to.be.equal('rgb(255, 165, 0)')
        resolve();
      });
    });
  });
  test('3Bind to a boolean attribute: expect h1 is display to none', async () => {
    const el = new LitElementBind();
    el.isHidden = true;
    container.appendChild(el);
    await new Promise((resolve) => {
      setTimeout(() => {
        const h1 = container.querySelector('lit-element-bind').shadowRoot.querySelector('h1');
        expect(getStyle(h1)('display')).to.be.equal('none')
        resolve();
      });
    });
  });
  test('4Bind to a property: expect input value as "is the real given value"', async () => {
    const value = 'is the real given value';
    const el = new LitElementBind();
    el.value = value;
    container.appendChild(el);
    await new Promise((resolve) => {
      setTimeout(() => {
        const input = container.querySelector('lit-element-bind').shadowRoot.querySelector('input');
        expect(input.value).to.be.equal(value)
        resolve();
      });
    });
  });
});