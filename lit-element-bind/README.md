# Lit Element - how is bind
Using [Lit Element 1.x](https://lit-element.polymer-project.org/)

First create a ```src``` folder and put a ```lit-element-bind.js``` file.
Create thre classes in the node style:
- ```error```  wich set color to **red**.
- ```warining```  wich set color to **orange**.
- ```hidden```  wich set display to **hidden**.


In Lit Element there is a diferent ways for bind data, in this exercise is using a ```h1``` and a ```input``` to test all (except bind to event this will be in other exercism).


- Bind to an attribute: using the attribute **comp-clase**.
- Bind to text content: using the attribute **comp-title**.
- Bind to a boolean attribute: using the property **isHidden**.
- Bind to a property: using to set input value.


## Setup

Go through the setup instructions for Javascript to install the necessary
dependencies:

[https://exercism.io/tracks/javascript/installation](https://exercism.io/tracks/javascript/installation)

## Requirements

Install assignment dependencies:

```bash
$ npm install 
```

## Making the test suite pass

Execute the tests with:

```bash
$ npm test
```


## Submitting Incomplete Solutions

It's possible to submit an incomplete solution so you can see how others have
completed the exercise.