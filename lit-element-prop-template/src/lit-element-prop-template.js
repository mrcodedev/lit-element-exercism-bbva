import {
  LitElement,
  html
} from 'lit-element';

export class LitElementPropTemplate extends LitElement {
  render() {
    return html `
      <h1>${this.title}</h1>
      <p>${this.text}</p>
    `;
  }

  static get properties() {
    return {
      title: {
        type: String
      },
      text: {
        type: String
      }
    };
  }

  constructor() {
    super();
    this.title = 'hi from Lit element';
    this.text = 'this is a test';
  }
}

customElements.define('lit-element-prop-template', LitElementPropTemplate);